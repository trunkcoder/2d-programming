﻿using UnityEngine;
using System.Collections;

public class GhostMove2 : MonoBehaviour {

	public float posX;
	public float posY;
	public float speed;

	// Use this for initialization
	void Start () {
		speed = 0.05f;
	}
	
	// Update is called once per frame
	void Update () {
		posX = transform.position.x + speed;
		posY = transform.position.y;
		transform.position = new Vector2 (posX, posY);
		
		if (posX > 7) {
			speed *= -1;
		}

		if (posX < -7) {
			speed *= -1;
		}
	}
}
