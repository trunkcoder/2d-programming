﻿using UnityEngine;
using System.Collections;

public class CrateScale : MonoBehaviour {

	public float scale;
	public float scaleRate;

	// Use this for initialization
	void Start () {
		scale = 1f;
		scaleRate = 0.01f;
	}
	
	// Update is called once per frame
	void Update () {
		transform.localScale = new Vector2 (scale, scale);
		scale += scaleRate;
	}
}
