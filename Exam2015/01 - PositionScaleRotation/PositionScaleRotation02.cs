﻿using UnityEngine;
using System.Collections;

public class PositionScaleRotation02 : MonoBehaviour {

	float posX;
	float posY;
	float speed = 0.05f;

	// Use this for initialization
	void Start () {
		posX = transform.position.x;
		posY = transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3(posX, posY, 0);
		posX += speed;
		if (posX > 7f) {
			speed *= -1;

		}
		if (posX < -7f) {
			speed *= -1;
			
		}
	}
}
