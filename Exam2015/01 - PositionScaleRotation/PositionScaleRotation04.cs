﻿using UnityEngine;
using System.Collections;

public class PositionScaleRotation04
: MonoBehaviour {

	float posX;
	float posY;
	float rotZ;
	float speed = 0.03f;
	float rotSpeed = -2f;

	// Use this for initialization
	void Start () {
		posX = transform.position.x;
		posY = transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {

		transform.position = new Vector3 (posX, posY, 0);
		posX += speed;
		transform.eulerAngles = new Vector3(0, 0, rotZ);
		rotZ += rotSpeed;
		if (posX > 7f) {
			posX = -7f;
		}
	}
}
