﻿using UnityEngine;
using System.Collections;

public class PositionScaleRotation03 : MonoBehaviour {

	float rotZ;
	float speed = 3f;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		transform.eulerAngles = new Vector3(0, 0, rotZ);
		rotZ += speed;

	}
}
