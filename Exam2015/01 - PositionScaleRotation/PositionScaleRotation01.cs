﻿using UnityEngine;
using System.Collections;

public class PositionScaleRotation01 : MonoBehaviour {

	float posX;
	float posY;
	float speed = 0.05f;

	// Use this for initialization
	void Start () {
		posX = transform.position.x;
		posY = transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3(posX, posY, 0);
		posY += speed;
		if (posY > 6f) {
			posY = -6f;
		}
	}
}
