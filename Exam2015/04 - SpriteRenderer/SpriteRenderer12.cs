﻿using UnityEngine;
using System.Collections;

public class SpriteRenderer12 : MonoBehaviour {

	public string resourceName;
	Sprite[] sprites;
	int spriteIndex = 0;

	// Use this for initialization
	void Start () {
		if (resourceName !="") {
			sprites = Resources.LoadAll<Sprite>(resourceName);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space)) {
			spriteIndex += 1;

			GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0,10)];

		}
	}
}
