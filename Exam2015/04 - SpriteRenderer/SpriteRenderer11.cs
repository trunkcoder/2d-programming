﻿using UnityEngine;
using System.Collections;

public class SpriteRenderer11 : MonoBehaviour {

	public string resourceName;
	Sprite[] sprites;
	int spriteIndex = 0;

	// Use this for initialization
	void Start () {
		if (resourceName !="") {
			sprites = Resources.LoadAll<Sprite>(resourceName);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.RightArrow)) {
			spriteIndex += 1;
			if (spriteIndex > 9) {
				spriteIndex = 0;
			}
			GetComponent<SpriteRenderer>().sprite = sprites[spriteIndex];

		}

		if (Input.GetKeyDown(KeyCode.LeftArrow)) {
			spriteIndex -= 1;
			if (spriteIndex < 0) {
				spriteIndex = 9;
			}
			GetComponent<SpriteRenderer>().sprite = sprites[spriteIndex];

		}
	}
}
