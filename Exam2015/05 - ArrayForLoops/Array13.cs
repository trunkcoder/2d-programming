﻿using UnityEngine;
using System.Collections;

public class Array13 : MonoBehaviour {

	GameObject[] balloons;
	int balloonCount = 0;

	// Use this for initialization
	void Start () {
		balloons = GameObject.FindGameObjectsWithTag ("balloon");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			balloons[balloonCount].transform.eulerAngles = new Vector3(0,0,45);
			balloonCount += 1;
			if (balloonCount > 5) {
				balloonCount = 0;
			}
		}
	}
}
