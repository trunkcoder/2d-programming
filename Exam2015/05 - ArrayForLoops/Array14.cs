﻿using UnityEngine;
using System.Collections;

public class Array14 : MonoBehaviour {

	GameObject[] balloons;
	float scaleXY = 1f;
	float scaleRate = 0.1f;

	// Use this for initialization
	void Start () {
		balloons = GameObject.FindGameObjectsWithTag ("balloon");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Space)) {
			for (int i = 0; i < 6; i++) {
				balloons[i].transform.localScale = new Vector3(scaleXY,scaleXY,0);
			}
			scaleXY += scaleRate;
		}
	}
}
