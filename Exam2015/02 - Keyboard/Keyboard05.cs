﻿using UnityEngine;
using System.Collections;

public class Keyboard05: MonoBehaviour {

	float posX;
	float posY;
	float speed = 0.03f;


	// Use this for initialization
	void Start () {
		posX = transform.position.x;
		posY = transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.RightArrow)) {
			transform.position = new Vector3 (posX, posY, 0);
			posX += speed;
			if (posX > 7f) {
				posX = -7f;
			}
		}

		if (Input.GetKey(KeyCode.LeftArrow)) {
			transform.position = new Vector3 (posX, posY, 0);
			posX -= speed;
			if (posX < -7f) {
				posX = 7f;
			}
		}

	}
}
