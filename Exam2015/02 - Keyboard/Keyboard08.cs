using UnityEngine;
using System.Collections;

public class Keyboard08 : MonoBehaviour {

	float posX;
	float posY;
	float speedX = 0.03f;
	float speedY = 0.03f;
	float directionX = 0;
	float directionY = 0;
	
	// Use this for initialization
	void Start () {
		posX = transform.position.x;
		posY = transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		
		transform.position = new Vector3 (posX, posY, 0);
		posX += speedX * directionX;
		posY += speedY * directionY;
		if (Input.GetKeyDown(KeyCode.RightArrow)) {
			
			directionX = 1f;
			directionY = 0;
		}
		
		if (Input.GetKey(KeyCode.LeftArrow)) {
			directionX = -1f;
			directionY = 0;
		}
		
		if (Input.GetKey(KeyCode.UpArrow)) {
			directionX = 0;
			directionY = 1f;
		}
		
		if (Input.GetKey(KeyCode.DownArrow)) {
			directionX = 0;
			directionY = -1f;
		}
		
		if (posX > 7f) {
			posX = -7f;
		}
		
		if (posX < -7f) {
			posX = 7f;
		}

		if (posY > 6f) {
			posY = -6f;
		}
		
		if (posY < -6f) {
			posY = 6f;
		}
		
	}
}
