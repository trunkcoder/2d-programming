﻿using UnityEngine;
using System.Collections;

public class Keyboard07: MonoBehaviour {

	float posX;
	float posY;
	float speedX = 0.03f;
	float directionX = 0;

	// Use this for initialization
	void Start () {
		posX = transform.position.x;
		posY = transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {

		transform.position = new Vector3 (posX, posY, 0);
		posX += speedX * directionX;

		if (Input.GetKeyDown(KeyCode.RightArrow)) {
			directionX = 1f;
		}

		if (Input.GetKey(KeyCode.LeftArrow)) {
			directionX = -1f;
		}

		if (posX > 7f) {
			posX = -7f;
		}

		if (posX < -7f) {
			posX = 7f;
		}

	}
}
