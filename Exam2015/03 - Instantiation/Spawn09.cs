﻿using UnityEngine;
using System.Collections;

public class Spawn09 : MonoBehaviour {

	public GameObject balloon;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space)) {
			Instantiate(balloon, new Vector3(Random.Range(-6f,6f),-6f,0),Quaternion.identity);
		}
	}
}
