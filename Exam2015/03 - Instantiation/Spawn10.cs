﻿using UnityEngine;
using System.Collections;

public class Spawn10 : MonoBehaviour {
	
	public GameObject balls;
	int ballCount = 0;

	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		ballCount += 1;
		if (ballCount > 60) {
			Instantiate(balls, new Vector3(Random.Range(-6f,6f),6f,0),Quaternion.identity);
			ballCount = 0;
		}
	}
}
