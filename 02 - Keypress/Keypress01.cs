﻿using UnityEngine;
using System.Collections;

public class Keypress01 : MonoBehaviour {

	public float posX;
	public float posY;
	public float speed;
	
	// Use this for initialization
	void Start () {
		speed = 0.05f;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.RightArrow)) {
			posX = transform.position.x + speed;
			posY = transform.position.y;
		}
		if (Input.GetKey (KeyCode.LeftArrow)) {
			posX = transform.position.x - speed;
			posY = transform.position.y;
		}
		if (Input.GetKey (KeyCode.UpArrow)) {
			posX = transform.position.x;
			posY = transform.position.y + speed;
		}
		if (Input.GetKey (KeyCode.DownArrow)) {
			posX = transform.position.x;
			posY = transform.position.y - speed;
		}

		transform.position = new Vector2 (posX, posY);

	}
}
