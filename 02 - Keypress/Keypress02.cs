﻿using UnityEngine;
using System.Collections;

public class Keypress02 : MonoBehaviour {

	public float scale;
	public float scaleRate;
	
	// Use this for initialization
	void Start () {
		scale = 1f;
		scaleRate = 0.01f;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.UpArrow)) {
			transform.localScale = new Vector2 (scale, scale);
			scale += scaleRate;
		}
		if (Input.GetKey (KeyCode.DownArrow)) {
			transform.localScale = new Vector2 (scale, scale);
			scale -= scaleRate;
		}
	}
}
