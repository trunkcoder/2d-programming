﻿using UnityEngine;
using System.Collections;

public class Array02 : MonoBehaviour {

	GameObject[] ghosts;
	int rotateZ;
	int rotateSpeed = 5;

	// Use this for initialization
	void Start () {
		ghosts = GameObject.FindGameObjectsWithTag ("Ghost");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Space)) {
			for (int i = 0; i < 4; i++){
				ghosts[i].transform.eulerAngles = new Vector3(0,0,rotateZ);
			}
			rotateZ += rotateSpeed;
		}
	}
}
