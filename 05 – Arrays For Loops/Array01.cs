﻿using UnityEngine;
using System.Collections;

public class Array01 : MonoBehaviour {

	GameObject[] ghosts;
	int ghostIndex = 0;

	// Use this for initialization
	void Start () {
		ghosts = GameObject.FindGameObjectsWithTag ("Ghost");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space)) {
			ghosts[ghostIndex].transform.localScale = new Vector3(3,3,1);
			ghostIndex += 1;
		}
	}
}
