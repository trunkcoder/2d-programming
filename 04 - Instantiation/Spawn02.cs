﻿using UnityEngine;
using System.Collections;

public class Spawn02 : MonoBehaviour {

	public GameObject objectToSpawn;
	public int count;
	
	// Use this for initialization
	void Start () {
		count = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space) && count < 10) {
			Instantiate(objectToSpawn, new Vector3(Random.Range(-7f, 7f),5,0),Quaternion.identity);
			count += 1;
		}
	}
}
