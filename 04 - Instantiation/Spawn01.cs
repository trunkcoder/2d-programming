﻿using UnityEngine;
using System.Collections;

public class Spawn01 : MonoBehaviour {

	public GameObject objectToSpawn;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			Instantiate(objectToSpawn, new Vector3(Random.Range(-7f, 7f),5,0),Quaternion.identity);
		}
	}
}
