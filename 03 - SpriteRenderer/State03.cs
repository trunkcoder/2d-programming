﻿using UnityEngine;
using System.Collections;

public class State03 : MonoBehaviour {

	Sprite[] faces;
	public string resourceName;

	
	// Use this for initialization
	void Start () {
		faces = Resources.LoadAll<Sprite>(resourceName);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			GetComponent<SpriteRenderer>().sprite = faces[Random.Range(0,7)];
		}
	}
}
