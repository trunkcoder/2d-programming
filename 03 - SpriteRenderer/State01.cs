﻿using UnityEngine;
using System.Collections;

public class State01 : MonoBehaviour {

	Sprite[] arrows;
	public string resourceName;

	// Use this for initialization
	void Start () {
		if (resourceName != "") {
			arrows = Resources.LoadAll<Sprite> (resourceName);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			GetComponent<SpriteRenderer>().sprite = arrows[3];
		}
		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			GetComponent<SpriteRenderer>().sprite = arrows[0];
		}
		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			GetComponent<SpriteRenderer>().sprite = arrows[2];
		}
		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			GetComponent<SpriteRenderer>().sprite = arrows[1];
		}
	}
}
